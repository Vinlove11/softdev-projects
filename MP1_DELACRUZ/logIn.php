<!DOCTYPE html>
<html>
<head>
  <title>Bootstrap Modal Login Form</title>
  <link rel="stylesheet" type="text/css" href="bootstrap.css">
</head>
<body>

<div class="container jumbotron text-center">
  <h3>University of the East</h3>
  <button type="button" class="btn btn-success" data-toggle="modal" data-target="#loginModal">LOGIN</button>
</div>

<div class="modal fade" role="dialog" id="loginModal">
  <div class="modal-dialog">
    <div class="modal-content">
      
      <div class="modal-header">
        
        <button type="button" class="close" data-dismiss="modal">&times;</button>
      </div>

      <div style="text-align: center; margin: 24px 0 9px 0; margin">
      <img src ="logog.png" alt ="ue logo" class ="logo" width ="90" height ="90">
      </div>

      <div class="modal-body" style="padding:10px 10px;">
        <form role="form">

        <div class="modal-body">
          
          <div class="form-group">
            <input type="text" name="username" class="form-control" placeholder="Username">
          </div>

          <div class="form-group">
            <input type="password" name="Password" class="form-control" placeholder="Password">
          </div>
        
          <button type="submit" class="btn btn-success btn-block">
          <span class="glyphicon glyphicon-off"></span>Login</button>

          <button type="submit" class="btn btn-danger btn-success btn-block btn-default" data-dismiss="modal">
          <span class="glyphicon glyphicon-remove"></span>Cancel</button><br>

            
        </form>
      </div>
     </div>
  </div>
    
</div>
  
</div>

<script src="jquery-3.3.1.min.js"></script>
<script src="bootstrap.min.js"></script>
<script src="bootstrap.js"></script>

</body>
</html>