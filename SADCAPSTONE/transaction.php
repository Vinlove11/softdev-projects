<html lang="en"><head>

  <title>SUPERADMIN STUDENTS</title>

  <!-- Custom fonts for this template-->
 <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="sb-admin-2.2.css" rel="stylesheet">


</head>

<body>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-file-alt"></i>
        </div>
        <div class="sidebar-brand-text mx-2">Inventory<sup>system</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="home.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-users"></i>
          <span>Accounts</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Accounts:</h6>
            <a class="collapse-item" href="students.php">Students</a>
            <a class="collapse-item" href="professor.php">Professors</a>
          </div>
        </div>
      </li>


       <li class="nav-item">
        <a class="nav-link" href="items.php">
          <i class="fas fa-fw fa-shopping-cart"></i>
          <span>Items</span></a>
      </li>

       <li class="nav-item">
        <a class="nav-link" href="records.php">
          <i class="fas fa-fw fa-clone"></i>
          <span>Records</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">


      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Transaction -->
      <li class="nav-item active">
        <a class="nav-link" href="transaction.php">
          <i class="fas fa-fw fa-bell"></i>
          <span>Transaction</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
    <div class="side">
      <!-- Main Content -->
      <div id="content">

         <!-- Topbar -->
         <nav class="navbar navbar-expand navbar-light bg-white topbar mb-3 static-top shadow">

           <h5>University of the East - Computer Engineering Department</h5>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

              <!-- Nav Item - User Information -->
              <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">Name of Admin</span>
               <img class="img-profile rounded-circle" src="superadmin.png">
               </a>

              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">


<!-- DataTales Example -->
<div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-danger">Records</h6>
            </div>
            <div class="card-body">
              
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row">
                    <div class="col-sm-12 col-md-10">
                     </div>
                        <div class="col-sm-12 col-md-2">
                          <div id="dataTable_filter" class="dataTables_filter"><br>
                            <div class="input-group">
                            <div class="input-group-append">
                           <input type="search" class="form-control form-control-sm" placeholder="Search" aria-controls="dataTable">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div></div></div>
                       </div></div>
                            <div class="row">
                              <div class="col-sm-12"><table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 57px;">DATE</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 62px;">STUDENT NUMBER</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 100px;">TRANSACTION</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 31px;">PROFESSOR</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 67px;">REMARKS</th></tr>
                  </thead>
                  <tbody>
                  <tr role="row" class="odd">
                      <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                          <td class="sorting_1"></td>
                      <td></td>
                      <td></td>
                      <td></td>
                      <td></td>
                        <tr role="row" class="odd">
                      
                      
                    </tr><tr role="row" class="even">
                       
                    </tr></tbody>

                </table></div></div>
                
              </div>
            </div>
          


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
    </div>
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <form action="logout_module_connection.php" method="POST">
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" name="logout">Logout</button>
        </div>
      </div>
    </div>
  </div>
  </form>
      
    </div>
    

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>




</body></html>