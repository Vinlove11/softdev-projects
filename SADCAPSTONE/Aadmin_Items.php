<!DOCTYPE html>
<html lang="en"><head>

  <title>SUPERADMIN STUDENTS</title>

  <!-- Custom fonts for this template-->
 <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="sb-admin-2.2.css" rel="stylesheet">

  </head>

<body>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-file-alt"></i>
        </div>
        <div class="sidebar-brand-text mx-2">Inventory<sup>system</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="Aadmin_Home.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapsePages" aria-expanded="true" aria-controls="collapsePages">
          <i class="fas fa-fw fa-users"></i>
          <span>Accounts</span>
        </a>
        <div id="collapsePages" class="collapse" aria-labelledby="headingPages" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Accounts:</h6>
            <a class="collapse-item" href="Aadmin_Students.php">Students</a>
            <a class="collapse-item" href="Aadmin_Professor.php">Professors</a>
        
          </div>
        </div>
      </li>


       <li class="nav-item active">
        <a class="nav-link" href="Aadmin_Items.php">
          <i class="fas fa-fw fa-shopping-cart"></i>
          <span>Items</span></a>
      </li>

       <li class="nav-item">
        <a class="nav-link" href="Aadmin_Records.php">
          <i class="fas fa-fw fa-clone"></i>
          <span>Records</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">


      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Transaction -->
      <li class="nav-item">
        <a class="nav-link" href="Aadmin_error.php">
          <i class="fas fa-fw fa-bell"></i>
          <span>Transaction</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
    <div class="side">
      <!-- Main Content -->
      <div id="content">

         <!-- Topbar -->
         <nav class="navbar navbar-expand navbar-light bg-white topbar mb-3 static-top shadow">

           <h5>University of the East - Computer Engineering Department</h5>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

              <!-- Nav Item - User Information -->
              <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">Name of admin</span>
               <img class="img-profile rounded-circle" src="superadmin.png">
               </a>

              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">


<!-- DataTales Example -->
        <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-danger">Item List</h6>
            </div>
            <div class="card-body">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row">
                      
                    <div class="col-sm-10 col-md-10">
                      
                        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#addItemModal">Add Item</button></div>
                      
                        <div class="col-sm-10 col-md-2">
                          <div id="dataTable_filter" class="dataTables_filter">
                            <div class="input-group">
                            <div class="input-group-append">
                            
                            <input type="search" class="form-control"  placeholder="Search..">
                                
                       <button type="button"  class="btn btn-danger" name="btn_search"><i class="fas fa-search"></i></button>
                                
                    </div></div></div>
                       </div>

                       <?php
                    //////PUTTING THE DATA INTO EACH ROW OF TABLE//////
                    $mysqli = new mysqli('localhost','root','','inventorysystem') or die(mysqli_error($mysqli));
                    $result = $mysqli->query("SELECT * FROM item_list") or die(mysqli_error($mysqli));
                       ?>
                       
                        <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered dataTable" width="100%" style="width: 100%;"><br>
                  <thead>
                    <tr role="row">
                      <th style="width: 440px;">CATEGORY NAME</th>
                      <th style="width: 410px;">QUANTITY</th>
                      <th style="width: 400px;">ACTION</th></tr>
                  </thead>
                  <tbody>

                          <?php
                          while($row = $result->fetch_assoc()):
                          ?>
               
                  <tr role="row" class="odd">
                      <td><?php echo $row ['Item_Name'];?></td>
                      <td><?php echo $row ['Quantity'];?></td>
                      <td>
                          <a><button type="submit" class="btn btn-success" data-target="#viewItemModal" data-toggle="modal">View</button></a>
                      <a>
                          
                          <button type="submit" class="btn btn-success btn_edit" name="btn_edit">/</button>
                       <button type="submit" class="btn btn-danger btn_delete">X</button>
                      </a>
                      </td>
                      </tr><tr role="row" class="even">
                    </tr>
                    <?php endwhile; ?>
                     
                    </tbody>

                </table></div>

        </div>
              </div>
                  </div>
                      </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
    </div>
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <form action="logout_module_connection.php" method="POST">
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="submit" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" name="logout">Logout</button>
        </div>
      </div>
    </div>
  </div>
</form>
      </div>
    </div>


  <!-- MODAL FOR ADDING STUDENT INFORMATION ONLY --> 
  <!-- Add student Modal -->
  <div class="modal fade"id="addItemModal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Item</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="admin_item_connection.php" method="POST">
                <div class="form-inline">
                   <input type="hidden" name="update" id="update">
                    
                    
                   
                   <input type="text"  class = "form-control"  name="item_name"  placeholder="Item Name" style="width: 100%;" required>   
                    
                    <input type="text"  class = "form-control"  name="item_name"  placeholder="Product Number" style="width: 100%;" required>  
                    
                     <div class="dropdown">
    <button class="btn btn-default dropdown-toggle" type="button" data-toggle="dropdown">Categories
    <span class="caret"></span></button>
    <ul class="dropdown-menu">
      <li class="dropdown-header">Dropdown header 1</li>
      <li><a href="#">HTML</a></li>
      <li><a href="#">CSS</a></li>
      <li><a href="#">JavaScript</a></li>
      <li class="divider"></li>
      <li class="dropdown-header">Dropdown header 2</li>
      <li><a href="#">About Us</a></li>
    </ul>
  </div>
                   
                  <br>
                  </div>
                  <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                 <button type="submit" class="btn btn-primary" name="btn_add">ADD</button>
               </div>
               </form>
            </div>
          </div>
        
        </div>
      </div>


       <!-- MODAL FOR Edit ONLY --> 
  <!-- Edit Item Modal -->
  <div class="modal fade" id="editItemModal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Save Item</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
          <form action="admin_item_connection.php" method="POST">
                <div class="form-inline">
                   <input type="hidden" name="save" id="save">
                   <input type="text"  class = "form-control"  id="item_name" name="itemname"   placeholder="Item Name" style="width: 100%;" required >
                   <input type="text"  class = "form-control"  id="quantity" name="quantity"  placeholder="Quantity" style="width: 100%;" required>
                  <br>
                  </div>
                  <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                 <button type="submit" class="btn btn-primary" name="btn_save">SAVE</button>
               </div>
               </form>
            </div>
          </div>
        
        </div>
      </div>
        
        
         <!-- MODAL FOR View ONLY --> 
  <!-- View Item Modal -->
  <div class="modal fade" id="viewItemModal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">View</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="" method="POST">
              
              
              
        
                      
                        <div class="col-sm-12 col-sm-10">
                            <table class="table table-bordered dataTable" width="100%" style="width: 100%;"><br>
                           
                  <thead>
                    <tr role="row">
                      <th style="width: 180px;">PRODUCT NUMBER</th>
                      <th style="width: 220px;">ITEM NAME</th>
                      <th style="width: 220px;">STATUS</th>
                      <th style="width: 100px;">ACTION</th></tr>
                  </thead>
                  <tbody>
                      
                      <tr role="row" class="odd">
                           <td></td>
                          <td></td>
                          <td></td>
                          <td></td>
                        <tr role="row" class="even">
                            <td></td>
                          <td></td>
                            <td></td>
                          <td></td>
                      </tr> </tbody></table>
              </div><br>
              
        
                  <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">CLOSE</button>
                 <button type="submit" class="btn btn-primary" name="btn_save">SAVE</button>
               </div>
               </form>
            </div>
          </div>
        
        </div>
      </div>

        <!-- MODAL FOR DELETING STUDENT INFORMMATION -->
        <div class="modal fade" id="deleteitem_modal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Delete Item Data</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <form action="admin_item_connection.php" method="POST">
                <div class="form-inline">
                  <input type="hidden" name="delete_item" id="delete_item">
                  <h5> Do you want to Delete this Data?</h5>
                  </div>
                  <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                 <button type="submit" class="btn btn-primary" name="btn_delete">YES</button>
               </div>
               </form>
            </div>
          </div>
        
        </div>
      </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

<!-- EDIT CODE IN JAVASCRIPT -->
<script>
$(document).ready(function(){
	$('.btn_edit').on('click', function(){
		$('#editItemModal').modal('show');

    $tr= $(this).closest('tr');
    var data = $tr.children("td").map(function(){
    return $(this).text();
    }).get();

    console.log(data);

    $('#save').val(data[0]);
    $('#item_name').val(data[1]);
    $('#quantity').val(data[2]);
    $('#status').val(data[3]);
    $('#specification').val(data[4]);
	});
});
</script>

<!-- DELETE CODE IN JAVASCRIPT -->
<script>
$(document).ready(function(){
	$('.btn_delete').on('click', function(){
		$('#deleteitem_modal').modal('show');

    $tr= $(this).closest('tr');
    var data = $tr.children("td").map(function(){
    return $(this).text();
    }).get();

    console.log(data);

    $('#delete_item').val(data[0]);
	});
});
</script>

<!-- SEARCH CODE -->
<script>

function searchFun(){
  var filter, myTable, tr, td, i, td1, td2, td3, txtValue3, txtValue, txtValue1, txtValue2;
   input = document.getElementById("searchbar");
   filter = input.value.toUpperCase();
	 myTable = document.getElementById("myTable");  
	 tr = myTable.getElementsByTagName("tr");

	for(i=0; i<tr.length; i++){
       td = tr[i].getElementsByTagName("td")[0];
       td1 = tr[i].getElementsByTagName("td")[1];
       td2 = tr[i].getElementsByTagName("td")[2];
       td3 = tr[i].getElementsByTagName("td")[3];
    if(td || td1 || td2 || td3){
      txtValue = td.textContent || td.innerText;
      txtValue1 = td1.textContent || td1.innerText;
      txtValue2 = td2.textContent || td2.innerText;
      txtValue3 = td3
      .textContent || td3.innerText;
      if(txtValue.toUpperCase().indexOf(filter) > -1 || txtValue1.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1 ||  txtValue3.toUpperCase().indexOf(filter) > -1){
      tr[i].style.display ="";
      }else{
			tr[i].style.display = "none";
		  }
		}
	}
}

</script>	 


</body></html>