<?php
//Login code----------------------------
$mysqli = new mysqli('localhost','root','','inventorysystem') or die(mysqli_error($mysqli));    
if(isset($_POST['btn_login']))
    {
          $ID_Number = $mysqli->real_escape_string($_POST['id_number']);
          $Password = $mysqli->real_escape_string($_POST['password']);
             $Login = $mysqli->query("SELECT First_Name, Last_Name, Password FROM admin_account WHERE ID_Number ='$ID_Number'") or die($mysqli->error);  
            if(mysqli_num_rows($Login) > 0)
            {
                while($row = mysqli_fetch_array($Login))
                {
                  if(sha1($Password) == $row['Password'])
                  {
                    header("location:home.php");
                  }else{ 
                    print "<script language='JavaScript'>
                    window.alert('Incorrect Password!')
                    window.location.href='login.php';
                    </SCRIPT>";
                  }
             
              }
            }else{
              print "<script language='JavaScript'>
              window.alert('Account does not exist!')
              window.location.href='login.php';
              </SCRIPT>";
            }
            } 
          
             
?>
<!DOCTYPE html>
<html lang="en">
<head>

  <title>Login</title>

 
  <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">

  <link href="sb-admin-2.1.css" rel="stylesheet">

</head>

<body>

  <div class="container">

 
    <div class="row justify-content-center">

      <div class="col-xl-10 col-lg-12 col-md-9">

        <div class="card o-hidden border-0 shadow-lg my-4">
          <div class="card-body p-0">
            
            <div class="row">
              <div class="col-lg-6 d-none d-lg-block">
                
              

<div id="demo" class="carousel slide" data-ride="carousel">
  
  <ul class="carousel-indicators">
    <li data-target="#demo" data-slide-to="0" class="active"></li>
    <li data-target="#demo" data-slide-to="1"></li>
    <li data-target="#demo" data-slide-to="2"></li>
  </ul>
  

  <div class="carousel-inner">
    <div class="carousel-item active">
      <img src="img_chania.jpg" alt="Los Angeles" width="600" height="680">
    </div>
    <div class="carousel-item">
      <img src="img_flower.jpg" alt="Chicago" width="600" height="680">
    </div>
    <div class="carousel-item">
      <img src="img_flower2.jpg" alt="New York" width="600" height="680">
    </div>
  </div>
  
  <a class="carousel-control-prev" href="#demo" data-slide="prev">
    <span class="carousel-control-prev-icon"></span>
  </a>
  <a class="carousel-control-next" href="#demo" data-slide="next">
    <span class="carousel-control-next-icon"></span>
  </a>
</div></div>

              <div class="col-lg-6">
                <div class="p-5">
                  <div class="text-center">
                    <div><img src ="cpe.png" class ="logo" width ="90" height ="90"></div>
    <hr><br></div>

          <form action="login.php" method="POST" class="user">
                <div class="form-group">
                    <input type="text" class="form-control form-control-user" name="id_number" placeholder="Enter ID Number" ></div>
                <div class="form-group">
                    <input type="password" class="form-control form-control-user" name="password" placeholder="Password" ></div>
        <div class="form-group">
          <div class="custom-control custom-checkbox small">
                    <input type="checkbox" class="custom-control-input" id="customCheck">
                        <label class="custom-control-label" for="customCheck">Remember Me</label></div>
                    <br>
                    <button type="submit" class="btn btn-primary btn-user btn-block" name="btn_login">Login</button>                   
                    
                  </form>
                  <hr>
    
                  <div class="text-center">
                    <a class="small" href="register.php<?php  ?>">Create an Account!</a>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>


  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>
  <script src="js/sb-admin-2.min.js"></script>
</body>
</html>