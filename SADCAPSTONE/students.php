<html lang="en"><head>

  <title>SUPERADMIN STUDENTS</title>

  <!-- Custom fonts for this template-->
 <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="sb-admin-2.2.css" rel="stylesheet">

</head>

<body>

<?php require_once 'student_module_connection.php';?>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-dark sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-file-alt"></i>
        </div>
        <div class="sidebar-brand-text mx-2">Inventory<sup>system</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="home.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-users"></i>
          <span>Accounts</span>
        </a>
        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Accounts:</h6>
            <a class="collapse-item text-danger active" href="students.php">Students</a>
            <a class="collapse-item" href="professor.php">Professors</a>
            <a class="collapse-item" href="admin.php">Administrators</a>
          </div>
        </div>
      </li>

     
       <li class="nav-item">
        <a class="nav-link" href="items.php">
          <i class="fas fa-fw fa-shopping-cart"></i>
          <span>Items</span></a>
      </li>

       <li class="nav-item">
        <a class="nav-link" href="records.php">
          <i class="fas fa-fw fa-clone"></i>
          <span>Records</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

     <!-- Nav Item - Transaction -->
      <li class="nav-item">
        <a class="nav-link" href="error503.php">
          <i class="fas fa-fw fa-bell"></i>
          <span>Transaction</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
    <div class="side">
      <!-- Main Content -->
      <div id="content">

         <!-- Topbar -->
         <nav class="navbar navbar-expand navbar-light bg-white topbar mb-3 static-top shadow">

           <h5>University of the East - Computer Engineering Department</h5>

            <!-- Topbar Navbar -->
            <ul class="navbar-nav ml-auto">

              <!-- Nav Item - User Information -->
              <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <span class="mr-2 d-none d-lg-inline text-gray-600 small">Super Admin</span>
               <img class="img-profile rounded-circle" src="superadmin.png">
               </a>

              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">


<!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-danger">Student List</h6>
            </div>
            <div class="card-body">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row">
                      
                    <div class="col-sm-12 col-md-10">
                      
                        <button type="button" class="btn btn-outline-danger" data-toggle="modal" data-target="#addStudentModal">Add Student</button></div>
                      
                        <div class="col-sm-10 col-md-2">
                          <div id="dataTable_filter" class="dataTables_filter">
                            <div class="input-group">
                            <div class="input-group-append">
                            
                            <input type="search" class="form-control"  placeholder="Search..">
                                
                       <button type="button"  class="btn btn-danger" name="btn_search"><i class="fas fa-search"></i></button>
                                
                    </div></div></div>
                       </div>
                       
                                          <?php
                    //////PUTTING THE DATA INTO EACH ROW OF TABLE//////
                    $mysqli = new mysqli('localhost','root','','inventorysystem') or die(mysqli_error($mysqli));
                    $result = $mysqli->query("SELECT * FROM student_account") or die(mysqli_error($mysqli));
                                          ?>

                   <div class="row">
                        <div class="col-sm-12">
                            <table class="table table-bordered dataTable" width="100%" style="width: 100%;"><br>
                           
                  <thead>
                    <tr role="row">
                      <th style="width: 180px;">STUDENT NUMBER</th>
                      <th style="width: 220px;">FIRST NAME</th>
                      <th style="width: 220px;">LAST NAME</th>
                      <th style="width: 350px;">UE WEB ADDRESS</th>
                      <th style="width: 175px;">CONTACT NUMBER</th>
                      <th style="width: 100px;">ACTION</th></tr>
                  </thead>
                  <tbody>


                          <?php
                          while($row = $result->fetch_assoc()):
                          ?>
                      <tr role="row" class="odd">
                      <td class="sorting_1"><?php echo $row ['ID_Number'];?></td>
                      <td><?php echo $row ['First_Name']; ?></td>
                      <td><?php echo $row ['Last_Name']; ?></td>
                      <td><?php echo $row ['Ue_Web_Address']; ?></td>
                      <td><?php echo $row ['Contact_Number']; ?></td>
                      <td>                   
                      <a><button type="submit" class="btn btn-success btn_edit" name="btn_edit">/</button>
                       <button type="submit" class="btn btn-danger btn_delete">X</button>
                      </a>
                      </td>
                    </tr><tr role="row" class="even">
                    </tr>
              
                      <?php endwhile; ?>
                     
                    </tbody>

                </table></div>

        </div>
              </div>
                  </div>
                      </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

     

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
    </div>
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>
        
 <!-- Logout Modal-->
 <form action="logout_module_connection.php" method="POST">
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="submit" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <button class="btn btn-primary" type="submit" name="logout">Logout</button>
        </div>
      </div>
    </div>
  </div>
</form>

 <!-- MODAL FOR ADDING STUDENT INFORMATION ONLY --> 
  <!-- Add student Modal -->
   <div class="modal fade"id="addStudentModal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Add Student</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
          <form action="student_module_connection.php" method="POST">
                <div class="form-inline">
                   <input type="text"  class = "form-control"  name="student_number"  placeholder="Student Number" maxlength="11" style="width: 100%;" required>
                   <input type="text"  class = "form-control"  name="first_name"   placeholder="First Name" style="width: 100%;" required >
                   <input type="text"  class = "form-control"   name="last_name"  placeholder="Last Name" style="width: 100%;" required>
                   <input type="text"  class = "form-control"  name="ue_web_address"  placeholder="UE Web Address" pattern="([A-Z0-9._%+-]+)@([U]+[E]+[.]+[E]+[D]+[U]+[.]+[P]+[H])$"  style="width: 100%;" required >
                   <input type="text"  class = "form-control"  name="contact_number"  placeholder="Contact Number" onkeypress=" digitvalidation(event)" style="width: 100%;" required>
                  <br>
                  </div>
                  <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal" name="btn_close">CLOSE</button>
                 <button type="submit" class="btn btn-primary" name="btn_add">ADD</button>
               </div>
               </form>
            </div>
          </div>
        
        </div>
      </div>
   

<!-- MODAL FOR EDITING STUDENT INFORMMATION -->
      <div class="modal fade"id="updateStudentModal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Edit Student Data</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
          <form action="student_module_connection.php" method="POST">
                <div class="form-inline">
                  <input type="hidden" name="update" id="update">
                   <input type="text"  class = "form-control" id="first_name" name="firstname"   placeholder="First Name" style="width: 100%;" required >
                   <input type="text"  class = "form-control"  id="last_name" name="lastname" placeholder="Last Name" style="width: 100%;" required>
                   <input type="email"  class = "form-control" id="ue_web_address" name="uewebaddress"  placeholder="UE Web Address" pattern="([A-Z.]+)@([U]+[E]+[.]+[E]+[D]+[U]+[.]+[P]+[H])$" style="width: 100%;" required >
                   <input type="text"  class = "form-control" id="contact_number" name="contactnumber"  placeholder="Contact Number" style="width: 100%;" required>
                  <br>
                  </div>
                  <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal" name="btn_close">CLOSE</button>
                 <button type="submit" class="btn btn-primary" name="btn_save">SAVE</button>
               </div>
               </form>
            </div>
          </div>
        
        </div>
      </div>


      <!-- MODAL FOR DELETING STUDENT INFORMMATION -->
      <div class="modal fade"id="deletemodal" role="dialog" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
       <div class="modal-dialog modal-lg">
      <!-- Modal content-->
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title">Delete Student Data</h5>
          <button type="button" class="close" data-dismiss="modal">&times;</button>
        </div>
        <div class="modal-body">
          <p>Some text in the modal.</p>
          <form action="student_module_connection.php" method="POST">
                <div class="form-inline">
                  <input type="hidden" name="delete" id="delete">
                  <h4> Do you want to Delete this Data?</h4>
                  </div>
                  <div class="modal-footer">
                 <button type="button" class="btn btn-secondary" data-dismiss="modal">NO</button>
                 <button type="submit" class="btn btn-primary" name="btn_delete">YES</button>
               </div>
               </form>
            </div>
          </div>
        
        </div>
      </div>
   <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>

<!-- EDIT CODE IN JAVASCRIPT -->
<script>
$(document).ready(function(){
	$('.btn_edit').on('click', function(){
		$('#updateStudentModal').modal('show');

    $tr= $(this).closest('tr');
    var data = $tr.children("td").map(function(){
    return $(this).text();
    }).get();

    console.log(data);

    $('#update').val(data[0]);
    $('#first_name').val(data[1]);
    $('#last_name').val(data[2]);
    $('#ue_web_address').val(data[3]);
    $('#contact_number').val(data[4]);
	});
});
</script>

<!-- DELETE CODE IN JAVASCRIPT -->
<script>
$(document).ready(function(){
	$('.btn_delete').on('click', function(){
		$('#deletemodal').modal('show');

    $tr= $(this).closest('tr');
    var data = $tr.children("td").map(function(){
    return $(this).text();
    }).get();

    console.log(data);

    $('#delete').val(data[0]);
	});
});
</script>

<!-- SEARCH CODE -->
<script>

function searchFun(){
  var filter, myTable, tr, td, i, td1, td2, td3, td4,txtValue, txtValue1, txtValue2, txtValue3,txtValue4 ;
   input = document.getElementById("searchbar");
   filter = input.value.toUpperCase();
	 myTable = document.getElementById("myTable");  
	 tr = myTable.getElementsByTagName("tr");

	for(i=0; i<tr.length; i++){
       td = tr[i].getElementsByTagName("td")[0];
       td1 = tr[i].getElementsByTagName("td")[1];
       td2 = tr[i].getElementsByTagName("td")[2];
       td3 = tr[i].getElementsByTagName("td")[3];
       td4 = tr[i].getElementsByTagName("td")[4];
    if(td || td1 || td2 || td3 || td4){
      txtValue = td.textContent || td.innerText;
      txtValue1 = td1.textContent || td1.innerText;
      txtValue2 = td2.textContent || td2.innerText;
      txtValue3 = td3.textContent || td3.innerText;
      txtValue4 = td4.textContent || td4.innerText;
      if(txtValue.toUpperCase().indexOf(filter) > -1 || txtValue1.toUpperCase().indexOf(filter) > -1 || txtValue2.toUpperCase().indexOf(filter) > -1 || txtValue3.toUpperCase().indexOf(filter) > -1 || txtValue4.toUpperCase().indexOf(filter) > -1){
      tr[i].style.display ="";
      }else{
			tr[i].style.display = "none";
		  }
		}
	}
}
</script>	  

<!-- contact number validation --> 
 <script type="application/javascript">
    function digitvalidation(evt) {
        var ch = String.fromCharCode(evt.which);
        if (!(/[0-9]/.test(ch))) {
            evt.preventDefault();
        }
    }

</script>	    
   
      </div>
    </div>

</body></html>