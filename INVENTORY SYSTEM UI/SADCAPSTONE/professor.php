<html lang="en"><head>

  <title>SUPERADMIN PROFESSORS</title>

  <!-- Custom fonts for this template-->
 <link href="vendor/fontawesome-free/css/all.min.css" rel="stylesheet" type="text/css">
  <link href="https://fonts.googleapis.com/css?family=Nunito:200,200i,300,300i,400,400i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
  <link href="sb-admin-2.1.css" rel="stylesheet">

</head>

<body>

  <!-- Page Wrapper -->
  <div id="wrapper">

    <!-- Sidebar -->
    <ul class="navbar-nav bg-gradient-primary sidebar sidebar-dark accordion" id="accordionSidebar">

      <!-- Sidebar - Brand -->
      <a class="sidebar-brand d-flex align-items-center justify-content-center" href="home.php">
        <div class="sidebar-brand-icon rotate-n-15">
          <i class="fas fa-file-alt"></i>
        </div>
        <div class="sidebar-brand-text mx-2">Inventory<sup>system</sup></div>
      </a>

      <!-- Divider -->
      <hr class="sidebar-divider my-0">

      <!-- Nav Item - Dashboard -->
      <li class="nav-item">
        <a class="nav-link" href="home.php">
          <i class="fas fa-fw fa-tachometer-alt"></i>
          <span>Dashboard</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Interface
      </div>

      <!-- Nav Item - Pages Collapse Menu -->
      <li class="nav-item">
        <a class="nav-link collapsed" href="#" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="true" aria-controls="collapseTwo">
          <i class="fas fa-fw fa-users"></i>
          <span>Accounts</span>
        </a>
        <div id="collapseTwo" class="collapse show" aria-labelledby="headingTwo" data-parent="#accordionSidebar">
          <div class="bg-white py-2 collapse-inner rounded">
            <h6 class="collapse-header">Custom Accounts:</h6>
            <a class="collapse-item" href="students.php">Students</a>
            <a class="collapse-item active" href="professor.php">Professors</a>
          </div>
        </div>
      </li>

     
       <li class="nav-item">
        <a class="nav-link" href="items.php">
          <i class="fas fa-fw fa-shopping-cart"></i>
          <span>Items</span></a>
      </li>

       <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-clone"></i>
          <span>Records</span></a>
      </li>

      <!-- Divider -->
      <hr class="sidebar-divider">

      <!-- Heading -->
      <div class="sidebar-heading">
        Addons
      </div>

      <!-- Nav Item - Charts -->
      <li class="nav-item">
        <a class="nav-link" href="charts.html">
          <i class="fas fa-fw fa-chart-area"></i>
          <span>Charts</span></a>
      </li>

      <!-- Nav Item - Tables -->
      <li class="nav-item">
        <a class="nav-link" href="tables.html">
          <i class="fas fa-fw fa-table"></i>
          <span>Tables</span></a>
      </li>


      <!-- Divider -->
      <hr class="sidebar-divider d-none d-md-block">

      <!-- Sidebar Toggler (Sidebar) -->
      <div class="text-center d-none d-md-inline">
        <button class="rounded-circle border-0" id="sidebarToggle"></button>
      </div>

    </ul>
    <!-- End of Sidebar -->

    <!-- Content Wrapper -->
    <div id="content-wrapper" class="d-flex flex-column">
    <div class="side">
      <!-- Main Content -->
      <div id="content">

        <!-- Topbar -->
        <nav class="navbar navbar-expand navbar-light bg-white topbar mb-4 static-top shadow">

          <h4>University of the East - Computer Engineering Department</h4>

          <!-- Topbar Navbar -->
          <ul class="navbar-nav ml-auto">


         <!-- Nav Item - User Information -->
            <li class="nav-item dropdown no-arrow">
              <a class="nav-link dropdown-toggle" href="#" id="userDropdown" role="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <span class="mr-2 d-none d-lg-inline text-gray-600 small">Super Admin</span>
                <img class="img-profile rounded-circle" src="superadmin.png">
              </a>
              <!-- Dropdown - User Information -->
              <div class="dropdown-menu dropdown-menu-right shadow animated--grow-in" aria-labelledby="userDropdown">
                <a class="dropdown-item" href="#">
                  <i class="fas fa-user fa-sm fa-fw mr-2 text-gray-400"></i>
                  Profile
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-cogs fa-sm fa-fw mr-2 text-gray-400"></i>
                  Settings
                </a>
                <a class="dropdown-item" href="#">
                  <i class="fas fa-list fa-sm fa-fw mr-2 text-gray-400"></i>
                  Activity Log
                </a>
                <div class="dropdown-divider"></div>
                <a class="dropdown-item" href="#" data-toggle="modal" data-target="#logoutModal">
                  <i class="fas fa-sign-out-alt fa-sm fa-fw mr-2 text-gray-400"></i>
                  Logout
                </a>
              </div>
            </li>

          </ul>

        </nav>
        <!-- End of Topbar -->

        <!-- Begin Page Content -->
        <div class="container-fluid">


<!-- DataTales Example -->
          <div class="card shadow mb-4">
            <div class="card-header py-3">
              <h6 class="m-0 font-weight-bold text-primary">Student List</h6>
            </div>
            <div class="card-body">
              <div class="table-responsive">
                <div id="dataTable_wrapper" class="dataTables_wrapper dt-bootstrap4">
                  <div class="row">
                    <div class="col-sm-12 col-md-10">
                      <div class="dataTables_length" id="dataTable_length">
                        <label>Show <select name="dataTable_length" aria-controls="dataTable" class="custom-select custom-select-sm form-control form-control-sm">
                          <option value="10">10</option>
                          <option value="25">25</option>
                          <option value="50">50</option>
                          <option value="100">100</option>
                        </select> entries</label></div></div>
                        <div class="col-sm-12 col-md-2">
                          <div id="dataTable_filter" class="dataTables_filter"><br>
                            <div class="input-group">
                            <div class="input-group-append">
                           <input type="search" class="form-control form-control-sm" placeholder="Search" aria-controls="dataTable">
                      <button class="btn btn-primary" type="button">
                        <i class="fas fa-search fa-sm"></i>
                      </button>
                    </div></div></div>
                       </div></div>
                            <div class="row">
                              <div class="col-sm-12"><table class="table table-bordered dataTable" id="dataTable" width="100%" cellspacing="0" role="grid" aria-describedby="dataTable_info" style="width: 100%;">
                  <thead>
                    <tr role="row">
                      <th class="sorting_asc" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-sort="ascending" aria-label="Name: activate to sort column descending" style="width: 57px;">ID NUMBER</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 62px;">PASSWORD</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Position: activate to sort column ascending" style="width: 62px;">FIRST NAME</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Office: activate to sort column ascending" style="width: 50px;">LAST NAME</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Age: activate to sort column ascending" style="width: 31px;">MIDDLE NAME</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Start date: activate to sort column ascending" style="width: 69px;">UE WEB ADDRESS</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 67px;">CONTACT NUMBER</th>
                      <th class="sorting" tabindex="0" aria-controls="dataTable" rowspan="1" colspan="1" aria-label="Salary: activate to sort column ascending" style="width: 67px;">ACTION</th></tr>
                  </thead>
                  <tfoot>
                    <tr>
                      <th rowspan="1" colspan="1">ID NUMBER</th>
                      <th rowspan="1" colspan="1">PASSWORD</th>
                      <th rowspan="1" colspan="1">FIRST NAME</th>
                      <th rowspan="1" colspan="1">LAST NAME</th>
                      <th rowspan="1" colspan="1">MIDDLE NAME</th>
                      <th rowspan="1" colspan="1">UE WEB ADDRESS</th>
                      <th rowspan="1" colspan="1">CONTACT NUMBER</th>
                      <th rowspan="1" colspan="1">ACTION</th></tr>
                  </tfoot>
                  <tbody>

               
                 <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                       <tr role="row" class="odd">
                      <td class="sorting_1">20110458964</td>
                      <td>Admin258</td>
                      <td>Denver</td>
                      <td>Moscow</td>
                      <td>Rio</td>
                      <td>DENVER123@UE.EDU.PH</td>
                      <td>09174544745</td>
                      <td>EDIT</td>
                    </tr><tr role="row" class="even">
                      
                    </tr></tbody>

                </table></div></div>
                <div class="row">
                  <div class="col-sm-12 col-md-5">
                    <div class="dataTables_info" id="dataTable_info" role="status" aria-live="polite">Showing 1 to 10 of 100 entries</div></div>
                    <div class="col-sm-12 col-md-7">
                      <div class="dataTables_paginate paging_simple_numbers" id="dataTable_paginate">
                        <ul class="pagination">
                          <li class="paginate_button page-item previous disabled" id="dataTable_previous">
                            <a href="#" aria-controls="dataTable" data-dt-idx="0" tabindex="0" class="page-link">Previous</a></li>
                            <li class="paginate_button page-item active">
                              <a href="#" aria-controls="dataTable" data-dt-idx="1" tabindex="0" class="page-link">1</a></li>
                              <li class="paginate_button page-item ">
                                <a href="#" aria-controls="dataTable" data-dt-idx="2" tabindex="0" class="page-link">2</a></li>
                                <li class="paginate_button page-item ">
                                  <a href="#" aria-controls="dataTable" data-dt-idx="3" tabindex="0" class="page-link">3</a></li>
                                  <li class="paginate_button page-item ">
                                        <li class="paginate_button page-item next" id="dataTable_next">
                                          <a href="#" aria-controls="dataTable" data-dt-idx="7" tabindex="0" class="page-link">Next</a></li></ul></div></div></div></div>
              </div>
            </div>
          </div>


        </div>
        <!-- /.container-fluid -->

      </div>
      <!-- End of Main Content -->

      <!-- Footer -->
      <footer class="sticky-footer bg-white">
        <div class="container my-auto">
          <div class="copyright text-center my-auto">
            <span>Copyright © Your Website 2019</span>
          </div>
        </div>
      </footer>
      <!-- End of Footer -->
    </div>
    </div>
    <!-- End of Content Wrapper -->

  </div>
  <!-- End of Page Wrapper -->

  <!-- Scroll to Top Button-->
  <a class="scroll-to-top rounded" href="#page-top">
    <i class="fas fa-angle-up"></i>
  </a>

  <!-- Logout Modal-->
  <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
          <button class="close" type="button" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">×</span>
          </button>
        </div>
        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
        <div class="modal-footer">
          <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
          <a class="btn btn-primary" href="login.html">Logout</a>
        </div>
      </div>
    </div>
  </div>

  <!-- Bootstrap core JavaScript-->
  <script src="vendor/jquery/jquery.min.js"></script>
  <script src="vendor/bootstrap/js/bootstrap.bundle.min.js"></script>

  <!-- Core plugin JavaScript-->
  <script src="vendor/jquery-easing/jquery.easing.min.js"></script>

  <!-- Custom scripts for all pages-->
  <script src="js/sb-admin-2.min.js"></script>




</body></html>